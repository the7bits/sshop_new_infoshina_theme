from lfs.catalog.settings import VARIANT, PRODUCT_WITH_VARIANTS
from lfs.catalog.models import Product
from reviews.models import Review
from django.db.models import Avg


def get_mark(item_type, item_id):

    if item_type.model == u'product':
        product = Product.objects.get(id=item_id)
        if product.sub_type == VARIANT:
            ids = [v['id'] for v in product.parent.get_variants().values('id')]
            ids.append(product.parent.id)
        elif product.sub_type == PRODUCT_WITH_VARIANTS:
            ids = [v['id'] for v in product.get_variants().values('id')]
            ids.append(product.id)
        else:
            ids = [product.id]
    else:
        ids = [item_id]
    aggregate = Review.objects.filter(
        content_type=item_type,
        content_id__in=ids,
        active=True).aggregate(Avg('score'))

    if aggregate['score__avg']:
        return aggregate['score__avg']
    else:
        return 0


def get_average_for_instance(instance):
    from lfs.catalog.settings import PRODUCT_WITH_VARIANTS, VARIANT
    from django.db.models import Count, Avg
    from django.contrib.contenttypes.models import ContentType
    ctype = ContentType.objects.get_for_model(instance)
    ids = []
    if ctype.name == u'Product':
        if instance.sub_type == PRODUCT_WITH_VARIANTS:
            ids.append(instance.id)
            ids.extend(instance.get_variants().values_list('id', flat=True))
        elif instance.sub_type == VARIANT:
            ids.append(instance.parent.id)
            ids.extend(instance.parent.get_variants().values_list(
                'id', flat=True))
        else:
            ids.append(instance.id)
    else:
        ids.append(instance.id)
    result = Review.objects.filter(
        content_type_id=ctype.id, content_id__in=ids, active=True)\
        .aggregate(count=Count('score'), avg=Avg('score'))

    return result['avg'], result['count']
