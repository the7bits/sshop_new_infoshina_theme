# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.template import RequestContext
from lfs.core.decorators import ajax_required
from lfs.catalog.models import Product
from lfs.caching.utils import lfs_get_object_or_404
from lfs.customer.utils import get_customer
from urlparse import urlparse
from django.utils import simplejson
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from portlets.models import PortletAssignment
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.http import Http404
from django import forms
from lfs.core.utils import get_default_shop
from lfs.customer.forms import RegisterForm
from django.shortcuts import render_to_response, redirect

from lfs.customer.models import (
    Customer,
    Address,
    Phone,
    EmailAddress,
)


@ajax_required
def get_products_statuses(request):
    if 'ids' in request.POST:
        ids = request.POST['ids']
        page_type = request.POST.get('page_type', '')
        ids = json.loads(ids)
        products = list(Product.objects.filter(
            id__in=ids).values_list(
            'id', 'status__name', 'status__show_buy_button',
            'status__css_class', 'status__description'))
        buy_button_html = render_to_string(
            'lfs/catalog/buy_button.html', RequestContext(request, {
                'page_type': page_type,
                'ids': ids
                }))
        status_name_html = render_to_string(
            'lfs/catalog/status_name.html', RequestContext(request))
        status_description_html = render_to_string(
            'lfs/catalog/status_description.html', RequestContext(request))
    else:
        products = []
        buy_button_html = ''
        status_name_html = ''
    response = json.dumps({
        'products': products,
        'buy_button_html': buy_button_html,
        'status_name_html': status_name_html
    })
    return HttpResponse(response)


def get_seo_for_pages_text(product):
    from seo_for_pages.models import SEOForPage
    from django.db.models import Q
    seo_text = ''
    try:
        seo_text = SEOForPage.objects.filter(
            Q(url__icontains=product.get_category().slug) &
            Q(url__icontains=product.filteroption_set.get(
                filter__identificator='brand').identificator))[0]\
            .get_meta_seo_text()
    except:
        pass
    return seo_text


@ajax_required
def get_product_seo_text(request):
    from lfs.catalog.settings import PRODUCT_WITH_VARIANTS, VARIANT

    data = {'seo_text': ''}
    if 'product_id' in request.POST:
        product_id = request.POST.get('product_id')
        try:
            product = Product.objects.get(id=product_id)
        except:
            product = None

        if not product:
            return HttpResponse(json.dumps(data))

        if product.sub_type == PRODUCT_WITH_VARIANTS:
            variants = product.variants.filter(
                active=True,
                status__is_visible=True)
            if variants:
                variant = variants[0]
                data['seo_text'] = get_seo_for_pages_text(variant)

        elif product.sub_type == VARIANT:
            parent_seo_text = product.parent.get_meta_seo_text()
            if not parent_seo_text:
                data['seo_text'] = get_seo_for_pages_text(product)
            else:
                data['seo_text'] = parent_seo_text
    return HttpResponse(json.dumps(data))


def add_product_view(request, product_id, portlet_assignment_id=None):
    ''' Add product to notebook
    '''
    show_modal_login = False
    if not request.user.is_authenticated():
        try:
            product = lfs_get_object_or_404(Product, pk=product_id)
        except Product.DoesNotExist:
            return HttpResponse(
                json.dumps('Error'), mimetype="application/json")
        show_modal_login = True
        if request.session.get('notebook', False):
            notebook_list = request.session['notebook']
            if product not in notebook_list:
                notebook_list.append(product)
                request.session['notebook'] = notebook_list
        else:
            notebook_list = []
            notebook_list.append(product)
            request.session['notebook'] = notebook_list
    else:
        customer = get_customer(request)
        if 'notebook' not in customer.extras:
            customer.extras['notebook'] = []
        customer.extras['notebook'] = list(set(
            customer.extras['notebook'] + [int(product_id)]))
        customer.save()

    if portlet_assignment_id is None:
        return HttpResponse(json.dumps({
            'ok': 'Ok',
            'show_modal_login': show_modal_login
            }), mimetype="application/json")
    else:
        return ajax_notebook_render(request, portlet_assignment_id)


def ajax_notebook_render(request, portlet_assignment_id):
    obj = PortletAssignment.objects.get(pk=portlet_assignment_id)
    notebook_list = []
    if request.user.is_authenticated():
        customer = get_customer(request)
        if 'notebook' in customer.extras:
            notebook_list = Product.objects.filter(
                id__in=customer.extras['notebook'][:5])
    else:
        if request.session.get('notebook', False):
            notebook_list = request.session['notebook']
            if notebook_list:
                notebook_list = notebook_list[:5]

    html = render_to_string(
        "lfs/portlets/notebook.html",
        RequestContext(
            request,
            {
                "title": obj.portlet.title,
                "notebook_list": notebook_list,
                'ajax': True,
                'slot_name': obj.slot.name,
            }
        )
    )
    response = simplejson.dumps({'html': html})
    return HttpResponse(response)


def remove_product_view(request, product_id):
    ''' Remove product from notebook
    '''
    if request.user.is_authenticated():
        customer = get_customer(request)
        if 'notebook' in customer.extras:
            try:
                customer.extras['notebook'].remove(int(product_id))
                customer.save()
            except:
                pass
    else:
        try:
            product = lfs_get_object_or_404(Product, pk=product_id)
        except Product.DoesNotExist:
            return HttpResponse(
                json.dumps('Error'), mimetype="application/json")
        if request.session.get('notebook', False):
            notebook_list = request.session['notebook']
            try:
                notebook_list.remove(product)
                request.session['notebook'] = notebook_list
                return redirect('/notebook/')
            except ValueError:
                pass

    return redirect('/notebook/')


def notebook_view(request, template_name='lfs/notebook/notebook.html'):
    from lfs.core.utils import get_default_shop

    if request.user.is_authenticated():
        customer = get_customer(request)
        notebook_list = Product.objects.filter(
            id__in=customer.extras.get('notebook', []))
    else:
        if request.session.get('notebook', False):
            notebook_list = request.session['notebook']
        else:
            notebook_list = []
    display_type = request.session.get('notebook_display_type', u'table')
    return render_to_response(template_name, RequestContext(request, {
        'notebook_list': notebook_list,
        'products_count': len(notebook_list),
        'display_type': display_type,
        'currency': get_default_shop().default_currency.abbr
    }))


def remove_all_products_view(request):
    if request.user.is_authenticated():
        customer = get_customer(request)
        if 'notebook' in customer.extras:
            customer.extras['notebook'] = []
            customer.save()
    else:
        if request.session.get('notebook', False):
            request.session['notebook'] = []
    return redirect('/')


def notebook_remove_product_ajax(request, portlet_assignment_id, product_id):
    ''' Remove product from comparison
        (Delete product id from user session)
    '''
    obj = PortletAssignment.objects.get(pk=portlet_assignment_id)
    notebook_list = []
    if request.user.is_authenticated():
        customer = get_customer(request)
        if 'notebook' in customer.extras:
            try:
                customer.extras['notebook'].remove(int(product_id))
                customer.save()
            except:
                pass
    else:
        if request.session.get('notebook', False):
            notebook_list = request.session['notebook']
            try:
                notebook_list.remove(Product.objects.get(id=product_id))
                request.session['notebook'] = notebook_list
            except ValueError:
                pass

    html = render_to_string(
        "lfs/portlets/notebook.html",
        RequestContext(
            request,
            {
                'comparison_list_portlet': obj.portlet,
                'notebook_list': notebook_list,
                'ajax': True,
                'slot_name': obj.slot.name,
            }
        )
    )
    response = simplejson.dumps({'html': html})
    return HttpResponse(response)


def remove_all_products_ajax(request, portlet_assignment_id):
    obj = PortletAssignment.objects.get(pk=portlet_assignment_id)
    if request.user.is_authenticated():
        customer = get_customer(request)
        if 'notebook' in customer.extras:
            customer.extras['notebook'] = []
            customer.save()
    else:
        if request.session.get('notebook', False):
            request.session['notebook'] = []
    html = render_to_string(
        "lfs/portlets/notebook.html",
        RequestContext(
            request,
            {
                'comparison_list_portlet': obj.portlet,
                'ajax': True,
                'slot_name': obj.slot.name,
            }
        )
    )
    response = simplejson.dumps({'html': html})
    return HttpResponse(response)


def get_products_ajax(request):
    ids = []
    append_id = ids.append
    if request.user.is_authenticated():
        customer = get_customer(request)
        ids = customer.extras.get('notebook', [])
    else:
        for p in request.session.get('notebook', []):
            append_id(p.id)
    return HttpResponse(simplejson.dumps({
        'products': ids}))


def modal_login(request):
    """Custom view to login or register/login a user.

    The reason to use a custom login method are:

      * validate checkout type
      * integration of register and login form

    It uses Django's standard AuthenticationForm, though.
    """

    # If only anonymous checkout is allowed this view doesn't exists :)
    if not getattr(settings, 'CUSTOMER_AUTH_ENABLED', True):
        raise Http404

    # print request.get_full_path()

    # Using Djangos default AuthenticationForm
    login_form = AuthenticationForm()
    login_form.fields["username"].label = \
        getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username'))
    login_form.fields["username"].widget = forms.TextInput(
        attrs={
            'placeholder':
            getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
        })
    if settings.CUSTOMER_AUTH_BY == 'phone':
        login_form.fields["username"].help_text = \
            getattr(settings, 'CUSTOMER_PHONE_HELP_TEXT', '')

    if request.POST.get("action") == "login":
        login_form = AuthenticationForm(data=request.POST)
        login_form.fields["username"].label = \
            getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username'))
        login_form.fields["username"].widget = forms.TextInput(
            attrs={
                'placeholder':
                getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
            })

        if login_form.is_valid():
            redirect_to = request.POST.get("next")
            # Light security check -- make sure redirect_to isn't garbage.
            if not redirect_to or '//' in redirect_to or ' ' in redirect_to:
                redirect_to = getattr(settings, 'LOGIN_REDIRECT_URL', '/')

            from django.contrib.auth import login
            login(request, login_form.get_user())

            messages.success(request, _(u"You have been logged in."))

            return HttpResponse()

    # Get next_url
    next_url = request.REQUEST.get("next")
    if next_url is None:
        next_url = request.META.get("HTTP_REFERER")
    if next_url is None:
        next_url = getattr(settings, 'LOGIN_REDIRECT_URL', '/')

    # Get just the path of the url.
    # See django.contrib.auth.views.login for more
    next_url = urlparse(next_url)
    next_url = next_url[2]

    try:
        login_form_errors = login_form.errors["__all__"]
    except KeyError:
        login_form_errors = None

    html = render_to_string('modal_login/form.html',
                            RequestContext(request, {
                                "login_form": login_form,
                            }))
    response = json.dumps({'html': html})
    return HttpResponse(response)


def modal_registration(request):
    shop = get_default_shop(request)

    # If only anonymous checkout is allowed this view doesn't exists :)
    if not getattr(settings, 'CUSTOMER_AUTH_ENABLED', True):
        raise Http404

    # print request.get_full_path()

    # Using Djangos default AuthenticationForm
    register_form = RegisterForm(request=request)
    if settings.CUSTOMER_AUTH_BY == 'phone':
        register_form.fields["username"].help_text = \
            getattr(settings, 'CUSTOMER_PHONE_HELP_TEXT', '')

    if request.POST.get("action") == "register":
        register_form = RegisterForm(request=request, data=request.POST)
        if register_form.is_valid():
            username = register_form.data.get('username', None)
            phone = register_form.data.get('phone', None)
            email = register_form.data.get('email', None)
            password = register_form.data.get("password_1")
            #  Create user
            if getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'email':
                email = username
            elif getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'phone':
                phone = username

            user = User.objects.create_user(
                username=username,
                email=email,
                password=password
            )
            # At this moment post_save signal creates the Customer

            address_fields = shop.address_form_fields.split(',')
            reg_fields = shop.registration_form_fields.split(',')

            if Customer.objects.filter(user=user).exists():
                customer = Customer.objects.get(user=user)
                register_form_data = {}
                for item in reg_fields:
                    if item not in address_fields:
                        value = register_form.cleaned_data.get(item)
                        if value:
                            register_form_data.update({item: value})

                customer.set_info(register_form_data)
                customer.save()

                address_data = {}
                for item in reg_fields:
                    if item in address_fields:
                        value = register_form.cleaned_data.get(item)
                        if value:
                            address_data.update({item: value})

                if address_data:
                    Address.objects.create(
                        customer=customer,
                        fields=address_data,
                        default=True)

                if phone:
                    if getattr(settings, 'REGISTRATION_VALIDATE_PHONE', True):
                        import phonenumbers
                        phone_obj = phonenumbers.parse(
                            phone,
                            getattr(
                                settings, 'CUSTOMER_PHONE_NUMBER_REGION', 'UA'),
                        )
                        phone_str = '%s%s' % (
                            phone_obj.country_code,
                            phone_obj.national_number,
                        )
                        phone_type = phonenumbers.number_type(phone_obj)
                        p_type = 0
                        if phone_type == 0:
                            p_type = 5  # Landline
                        elif phone_type == 1:
                            p_type = 0  # Mobile
                    else:
                        phone_str = str(phone)
                        p_type = 0

                    Phone.objects.create(
                        customer=customer,
                        number=phone_str,
                        default=True,
                        type=p_type,
                    )

                if email:
                    EmailAddress.objects.create(
                        customer=customer,
                        email=email,
                        default=True,
                        verified=False,
                    )

            # Log in user
            from django.contrib.auth import authenticate
            from django.contrib.auth import login
            user = authenticate(username=username, password=password)
            login(request, user)
            redirect_to = request.POST.get("next")
            if not redirect_to or '//' in redirect_to \
                    or ' ' in redirect_to:
                redirect_to = getattr(settings, 'REGISTER_REDIRECT_URL', '/')

            messages.success(
                request, _(u"You have been registered and logged in."))
            return HttpResponse()

    # Get next_url
    next_url = request.REQUEST.get("next")
    if next_url is None:
        next_url = request.META.get("HTTP_REFERER")
    if next_url is None:
        next_url = getattr(settings, 'LOGIN_REDIRECT_URL', '/')

    # Get just the path of the url.
    # See django.contrib.auth.views.login for more
    next_url = urlparse(next_url)
    next_url = next_url[2]

    html = render_to_string('modal_registration/form.html',
                            RequestContext(request, {
                                "register_form": register_form,
                            }))
    response = json.dumps({'html': html})
    return HttpResponse(response)


@ajax_required
def notebook_add_to_cart(request):
    from lfs.cart.utils import (
        get_cart,
        get_or_create_cart,
    )
    import locale
    from lfs.core.signals import cart_changed
    from django.http import HttpResponse
    import json
    products_data = json.loads(request.POST.get('products', u'{}'))
    cart = get_cart(request)
    if cart is not None:
        cart.delete()
    cart = get_or_create_cart(request)
    for product_id, product_data in products_data.iteritems():
        try:
            product = Product.objects.get(pk=int(product_id))
        except:
            continue
        quantity = product_data.get('quantity')
        # Only active and deliverable products can be added to the cart.
        if (product.is_active() and product.status.show_buy_button) is False:
            continue
        try:
            if isinstance(quantity, unicode):
                quantity = quantity.encode("utf-8")
            quantity = int(abs(locale.atof(quantity)))
        except (TypeError, ValueError):
            quantity = 1

        cart.add(product, quantity)
    cart_changed.send(cart, request=request)
    cart.save()
    return HttpResponse(json.dumps({'success': True}))


@ajax_required
def notebook_remove_checked_products(request):
    products_ids = [
        int(pid)
        for pid in json.loads(request.POST.get('products', u'[]'))
    ]
    notebook_list = []
    if request.user.is_authenticated():
        customer = get_customer(request)
        if 'notebook' in customer.extras:
            try:
                customer.extras['notebook'] = [
                    pid for pid in customer.extras['notebook']
                    if pid not in products_ids]
                customer.save()
            except:
                pass
    else:
        if request.session.get('notebook', False):
            notebook_list = request.session['notebook']
            try:
                request.session['notebook'] = [
                    item for item in notebook_list
                    if item.id not in products_ids
                ]
            except ValueError:
                pass

    return HttpResponse(json.dumps({'success': True}))


def notebook_set_page_mode(request):
    from django.http import HttpResponseRedirect, HttpResponseForbidden
    page_mode = request.GET.get("notebook_display_type", 'list')
    if page_mode is None:
        return HttpResponseForbidden(
            _(u'Argument "mode" expected.'))

    if page_mode in ['list', 'table']:
        request.session["notebook_display_type"] = page_mode

    referrer = request.META.get("HTTP_REFERER")
    if referrer is not None:
        return HttpResponseRedirect(referrer)
    else:
        return HttpResponseForbidden(
            _(u'HTTP_REFERER is required.'))
