# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from adminconfig.utils import BaseConfig
from suit_ckeditor.widgets import CKEditorWidget


class InfoshinaThemeConfigForm(forms.Form):
    buy_button_template = forms.CharField(
        label=_(u'Buy button on product page template'),
        widget=CKEditorWidget,)
    buy_button_on_category_template = forms.CharField(
        label=_(u'Buy button on category page template'),
        widget=CKEditorWidget,)
    buy_button_on_model_template = forms.CharField(
        label=_(u'Buy button on model template'),
        widget=CKEditorWidget,)
    product_not_available_template = forms.CharField(
        label=_(u'Product not available text template'),
        widget=CKEditorWidget,)
    buy_on_credit_button_template = forms.CharField(
        label=_(u'Buy on credit button template on product page'),
        widget=CKEditorWidget,)
    credit_processing_button_template = forms.CharField(
        label=_(u'Credit processing button template on thank you page'),
        widget=CKEditorWidget,)
    show_credit_buttons = forms.BooleanField(
        label=_(u'Show by on credit button'),
        required=False)
    all_variants_button_template = forms.CharField(
        label=_(u'All variants button template'),
        widget=CKEditorWidget,)
    esender_default_template = forms.CharField(
        label=_(u'Esender template'),
        widget=CKEditorWidget
    )
    esender_product_template = forms.CharField(
        label=_(u'Esender template for product page'),
        widget=CKEditorWidget
    )
    one_click_order_button_template = forms.CharField(
        label=_(u'One click order button template'),
        widget=CKEditorWidget,
    )


class InfoshinaThemeConfig(BaseConfig):
    form_class = InfoshinaThemeConfigForm
    block_name = 'new_infoshina_theme'

    def __init__(self):
        super(InfoshinaThemeConfig, self).__init__()

        default_buy_button_template = \
            '''<p><button class="btn button_add_cart">Купить</button></p>'''
        default_product_not_available_template = '''
            <p><span class="btn disabled" style="font-size: 11px;">
            Нет в наличии</span></p>'''
        buy_on_credit_button_template =\
            '''<button id="button_add_cart_credit"
            class="btn credit">Купить в кредит</button>'''
        credit_processing_button_template =\
            '''<p>Для перехода на страницу банка и дальнейшего оформления
            кредита нажмите на кнопку</p><button class="credit_btn btn">
            Оформление кредита</<button>
            '''
        all_variants_button_template =\
            '''<a href="{{ link }}" class="btn ajax-load"
            data-container="#variants-{{ pk }}">
            <p>Все варианты</p></a>'''
        esender_template = '''
<form action="/subscribe-abonent/" id="subscribe_abonent_{{ portlet_id }}" method="POST">
<p>{% csrf_token %}</p>

<p>{{ form.media }}</p>

<p><input id="id_email" maxlength="200" name="email" placeholder="Почта" type="text" /></p>

<p><input id="id_name" maxlength="200" name="name" placeholder="Имя" type="text" />&nbsp;</p>

<p>&nbsp;<input id="subscribe_abonent_submit_{{ portlet_id }}" type="submit" value="Подписаться" /></p>
</form>

        '''
        one_click_order_button_template = '''
        {% if not is_set %}
         <a id="buy_one_click" type="button"
         href="{% url make_one_click_order product_id %}"
         class="btn btn-primary modal-link">
            Купить в один клик
         </a>
         <script type="text/javascript">
         $('#buy_one_click').click(function(){
            quan = $('input[name="quantity"]')[0].value
            this.href += '?amount=' + quan
         });
         </script>
        {% else %}
        <a id="buy_one_click" type="button"
        href="{% url make_one_click_set_order %}?{{ params }}"
        class="btn btn-primary modal-link">
            Купить в один клик
        </a>
        {% endif %}
        '''

        self.default_data = {
            'INFOSHINA_THEME_BUY_BUTTON_TEMPLATE': default_buy_button_template,
            'INFOSHINA_THEME_BUY_BUTTON_ON_CATEGORY_TEMPLATE':
                default_buy_button_template,
            'INFOSHINA_THEME_BUY_BUTTON_ON_MODEL_TEMPLATE':
                default_buy_button_template,
            'INFOSHINA_THEME_PRODUCT_NOT_AVAILABLE_TEMPLATE':
            default_product_not_available_template,
            'INFOSHINA_THEME_BUY_ON_CREDIT_BUTTON_TEMPLATE':
            buy_on_credit_button_template,
            'INFOSHINA_THEME_CREDIT_PROCESSING_BUTTON_TEMPLATE':
            credit_processing_button_template,
            'INFOSHINA_THEME_SHOW_CREDIT_BUTTONS': False,
            'INFOSHINA_THEME_ALL_VARIANTS_BUTTON_TEMPLATE':
            all_variants_button_template,
            'INFOSHINA_THEME_ESENDER_TEMPLATE': esender_template,
            'INFOSHINA_THEME_ESENDER_PRODUCT_TEMPLATE': esender_template,
            'INFOSHINA_THEME_ONE_CLICK_ORDER_BUTTON_TEMPLATE':
            one_click_order_button_template,
        }

        self.option_translation_table = (
            ('INFOSHINA_THEME_BUY_BUTTON_TEMPLATE', 'buy_button_template'),
            ('INFOSHINA_THEME_BUY_BUTTON_ON_CATEGORY_TEMPLATE',
                'buy_button_on_category_template'),
            ('INFOSHINA_THEME_BUY_BUTTON_ON_MODEL_TEMPLATE',
                'buy_button_on_model_template'),
            (
                'INFOSHINA_THEME_PRODUCT_NOT_AVAILABLE_TEMPLATE',
                'product_not_available_template'
            ),
            (
                'INFOSHINA_THEME_BUY_ON_CREDIT_BUTTON_TEMPLATE',
                'buy_on_credit_button_template'
            ),
            (
                'INFOSHINA_THEME_CREDIT_PROCESSING_BUTTON_TEMPLATE',
                'credit_processing_button_template'
            ),
            ('INFOSHINA_THEME_SHOW_CREDIT_BUTTONS', 'show_credit_buttons'),
            (
                'INFOSHINA_THEME_ALL_VARIANTS_BUTTON_TEMPLATE',
                'all_variants_button_template'
            ),
            ('INFOSHINA_THEME_ESENDER_TEMPLATE', 'esender_default_template'),
            (
                'INFOSHINA_THEME_ESENDER_PRODUCT_TEMPLATE',
                'esender_product_template'
            ),
            (
                'INFOSHINA_THEME_ONE_CLICK_ORDER_BUTTON_TEMPLATE',
                'one_click_order_button_template'
            ),
        )
