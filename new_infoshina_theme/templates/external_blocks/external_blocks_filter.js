<script type="text/javascript">
    function external_change(id, category){
        $('#external_blocks_filter_' + id + ' .external_filter_option_select').unbind('change');
        $('#external_blocks_filter_' + id + ' .external_filter_option_select').change(function(){
            data = { category_slug: category, changed: this.name }
            $('#external_blocks_filter_' + id + ' .external_filter_option_select').each(function(i, el){
                data[el.name] = el.value
            });
            $.ajax({
                type: "POST",
                url: '/external-blocks-filter/' + id,
                data: data,
                success:function(data){
                    data = $.parseJSON(data);
                    for (var html in data["html"])
                        $(data["html"][html][0]).html(data["html"][html][1]);
                    $('#external_blocks_filter_' + id + ' .external_filter').attr('href', data['button_link'])
                    external_change(id, category);
                    initCustomForms();
                }
            })
        });
    };
    external_change("{{ id }}", "{{ category_slug }}");
    $('#external_blocks_filter_' + {{ id }} + ' .clear_external_filter').click(function(){
        $('#external_blocks_filter_' + {{ id }} + ' .external_filter_option_select').each(function(i, el){
            el.value = "none"
        });
        $('#external_blocks_filter_' + {{ id }} + ' .external_filter_option_select').change();
        initCustomForms();
    });
    {% if slot_name == "Left" %}config.left_portlet_count++;{% endif %}
    {% if slot_name == "Right" %}config.right_portlet_count++;{% endif %}
</script>