$(function(){
    $.ajax({
        method: 'POST',
        url: '/maintenance-messages/',
        dataType: 'JSON',
    }).success(function(data){
        $('#maintenance-block').html(data.html);
        $(function() {
            $('.maintenance-messages-push').height($('.maintenance-messages-wrapper').height());
            $('#product-variants,#top-text-wrapper').css('padding-top',$('.maintenance-messages-wrapper').height());
        });
    });
}); 
