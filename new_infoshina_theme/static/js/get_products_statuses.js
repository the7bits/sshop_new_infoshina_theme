$(function(){
    var products = $('.js-buy-button');
    var page_type = $('.js-buy-button #page-type-input').val();
    var ids = [];
    for(i=0; i<products.length; i++){
        ids.push(products.eq(i).data('id'));
    }
    $.ajax({
        type: 'POST',
        url: "/get-products-statuses/",
        data: {
            'ids': JSON.stringify(ids),
            'page_type': page_type
        },
        dataType: "json",
        success: function(msg){
            products = msg['products'];
            for(i=0; i<products.length; i++){
                id = products[i][0];
                name = products[i][1];
                show_buy_button = products[i][2];
                css_class = products[i][3];
                description = products[i][4];
                if(show_buy_button) $('.js-buy-button[data-id='+id+']').html(msg['buy_button_html']);
                else $('.js-buy-button[data-id='+id+']').html(msg['status_name_html']);
                $('input[name=product_id]', '.js-buy-button[data-id='+id+']').val(id);
                $('.js-status-description[data-id='+id+']').attr('data-content', description).attr('data-original-title', name).text(name);
            }
        },
    });
});