$(function(){
	setTimeout( function() {

		var $seoText = $( '#top-text-wrapper' ).text();
		$seoText = $seoText.trim().length;
		if(!$seoText){
			$('.description-model').hide();
		}else{
			$('.description-model').show();
		}


		if($('#product-variants').length){
			var $productVariants = $('#product-variants').offset().top;
			$('.choose-size').click(function(){
				$("html, body").animate({ scrollTop: $productVariants }, 500);
			});
		};


		if ($('#top-text-wrapper').length) {
			var $descriptionModel = $('#top-text-wrapper').offset().top;
			$('.description-model').click(function(){
				$("html, body").animate({ scrollTop: $descriptionModel }, 700);
			});
		};


		// $('.scroll_to_reviews').click(function(){
		// 	var $serviceMessage = $('.maintenance-messages-wrapper');
		// 	var heightToReviews = $('.span12.reviews').offset().top;
		// 	if( $serviceMessage.length ){
		// 		var heightToReviews = heightToReviews -  $serviceMessage.height();
		// 	};
		// 	$("html, body").animate({ scrollTop: heightToReviews }, 700);
		// });

	},2000);
	$.ajax({
	    type: 'GET',
	    url: "/modal-login/",
	    dataType: "json",
	    success: function(msg){
	        $('.modal-login .login-form').html(msg['html']);
	    }
	});
	// end of 'modal login'


	// 'modal registration'
	$.ajax({
	    type: 'GET',
	    url: "/modal-registration/",
	    dataType: "json",
	    success: function(msg){
	        $('.modal-login .registration-form').html(msg['html']);
	    },
	});

	$.ajax({
        url: "/comparison/get-products",
        dataType: "json",
        success: function(data){
            for (var i=0; i < data['products'].length; i++){
                $("#compare_"+data['products'][i]).text("Добавлен к сравнению").attr("href", '/comparison/');
            };
        }
    });
    $.ajax({
        url: "/notebook/get-products",
        dataType: "json",
        success: function(data){
            for (var i=0; i < data['products'].length; i++){
                $("#notebook_"+data['products'][i]).text("В избранном").attr("href", '/notebook/');
            };
        }
    });
    $('.notebook-icon').click(function(){
    	waitFor(50,function(){
			$('.login-form .control-group #id_username').first().inputmask({
			    mask: "38 (999) 999-99-99",
			    showMaskOnHover: false
			});
			$('.registration-form .control-group #id_username').first().inputmask({
			    mask: "38 (999) 999-99-99",
			    showMaskOnHover: false
			});
    	});
    });

});
function waitFor(ms, callback){
   while($('.login-form .form-actions').length != 2){
       setTimeout(function(){

           waitFor(ms, callback);
       }, ms);
       return;
   }
   callback();
};
