$("div.filter_option input:checkbox, div.filter-options select").change("click", function(event){
    var selectOffset = $(this).is('select') ? 10 : 0;
    parent = $('div.filter-block').last();
    if ($('#apply-filter').length == 0){
        parent.after('<div id="apply-filter"></div>');
        $('div#apply-filter').hide();
        if($(".maintenance-messages-wrapper").length > 0){
            var $height = $(".maintenance-messages-wrapper").height();
            var margin = -37 - $height;
             $('div#apply-filter').css('margin-top',margin);
        }
            h = $(this).offset().top + window.button_offset + selectOffset;
        $('div#apply-filter').css('top',h);
        $('div#apply-filter').load('/apply/apply-filter/', function() {
            $('div#apply-filter').show("slow");
            $('#apply-filter-button').click(function() {
                mediator.publish('submit_filters');
            });
        });
    }
    else
    {
        h = $(this).offset().top + window.button_offset + selectOffset;
        $("div#apply-filter").animate({
        top:h,
      }, "slow");
    }
});