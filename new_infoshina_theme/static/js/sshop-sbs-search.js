/*
 * Step-by-step search plugin for P-Cart
 */


function reload_stepbystep_form(elements, portlet_id, portlet_model){

    elements.unbind("change")
    initCustomForms();
    elements.change(function(){
        var step_by_step_div = "#step-by-step-" + portlet_id
        $(step_by_step_div + " select").prop('disabled', 'disabled');
        var data = [];

        for (var i=0; i<$(step_by_step_div + " select").length; i++){
           data[i] = [$(step_by_step_div + " select")[i].name, $(step_by_step_div + " select")[i].value];
        }

        $.ajax({
            url: "/step-by-step/change-form/",
            type: "post",
            dataType: "json",
            data: {
                "selects": JSON.stringify(data),
                "portlet_id": portlet_id,
                "portlet_model": portlet_model,
            },
            success: function(msg) {
                $(step_by_step_div).html(msg["new_form"]);
                $(step_by_step_div + " select").change(reload_stepbystep_form($(step_by_step_div + " select"), portlet_id, portlet_model));

                if (msg["url"] && window.location.pathname != msg["url"]){
                    $(step_by_step_div)+$('.disabled.btn').remove();
                    var link = '<a href='+ msg["url"] +' class="step-by-step accept">Подобрать</a>';
                    $(step_by_step_div).append(link);
                }
                initCustomForms();
            }
        });
    });
}