# coding: utf-8
from django.http import HttpResponsePermanentRedirect, HttpResponseGone
from django.core.urlresolvers import is_valid_path
from django.conf import settings
from sbits_redirects.models import SbitsRedirect as Redirect
from lfs.filters.models import Filter
from urllib import unquote


class InfoshinaRedirectMiddleware(object):
    def process_response(self, request, response):
        try:
            path = unquote(
                request.get_full_path().encode('utf8')).decode('utf8')
        except:
            path = request.get_full_path()
        if '/shiny/' in path:
            filter_obj = Filter.objects.get(identificator="ship")
            fo_set = [
                x.identificator for x in filter_obj.filteroption_set.all()]
            for fo in fo_set:
                if '/shiny/' + fo in path:
                    new_path = path.replace(
                        '/shiny/' + fo, '/shiny/zimnie/' + fo)
                    return HttpResponsePermanentRedirect(new_path)
        return response


class Redirect404Middleware(object):
    def process_response(self, request, response):
        old_url = request.get_full_path()
        while '//' in old_url:
            old_url = old_url.replace('//', '/')
        new_url = old_url
        urlconf = getattr(request, 'urlconf', None)
        path = request.path_info
        check = False
        if old_url.endswith('/'):
            new_path = request.path_info.rstrip('/')
        else:
            check = True
            new_path = request.path_info + '/'
        if (not is_valid_path(path, urlconf)) and\
                is_valid_path(new_path, urlconf):
            r = Redirect.objects.filter(
                site__id__exact=settings.SITE_ID, old_path=old_url)
            if r.exists():
                return HttpResponsePermanentRedirect(r[0].new_path)\
                    if r[0].new_path\
                    else HttpResponseGone()
            else:
                new_url = old_url.rstrip('/') if not check else old_url + '/'
        r = Redirect.objects.filter(
            site__id__exact=settings.SITE_ID, old_path=new_url)
        if r.exists():
            new_url = r[0].new_path
            return HttpResponsePermanentRedirect(r[0].new_path)\
                if r[0].new_path\
                else HttpResponseGone()
        if new_url != old_url:
            return HttpResponsePermanentRedirect(new_url)
        return response
