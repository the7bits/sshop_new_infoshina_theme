��          �            x     y  $   �     �  #   �  -   �  3   *     ^  !   o     �     �     �  #   �     �  3     !   D  �  f  1     O   M  :   �  I   �  Y   "  v   |  .   �  Q   "     t     �  ;   �  H   �  >   1  e   p  N   �                                                                 
   	           All variants button template Buy button on category page template Buy button on model template Buy button on product page template Buy on credit button template on product page Credit processing button template on thank you page Esender template Esender template for product page Meta keywords New infoshina theme One click order button template Product not available text template Show by on credit button Soon you will get an order confirmation via E-Mail. of users found this review useful Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-11 11:32+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Шаблон кнопки все варианты Шаблон кнопки купить на странице категории Шаблон кнопки купить для модели Шаблон кнопки купить на странице товара Шаблон кнопки купить в кредит на странице товара Шаблон кнопки оформления кредита на странице Спасибо за покупку Шаблон формы подписаться Шаблон формы подписаться на странице товара Заголовок 2 МЕТА Тема Инфошины Шаблон кнопки купить в один клик Шаблон текста о недоступности продукта Показывать кнопку купить в кредит Вы получите подтверждение заказа на электронную почту. пользователей считают этот отзыв полезным 