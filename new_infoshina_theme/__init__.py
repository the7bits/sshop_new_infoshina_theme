# coding: utf-8


def update_settings(inherited_settings):
    from django.utils.translation import ugettext_lazy as _
    new_settings = {}
    new_settings['AVAILABLE_SHOP_THEMES'] =\
        inherited_settings['AVAILABLE_SHOP_THEMES'] + (
            ('new_infoshina_theme', u'new_infoshina theme'),
        )
    new_settings['INSTALLED_APPS'] = inherited_settings['INSTALLED_APPS'] + (
        'new_infoshina_theme',
    )
    if 'ADMIN_CONFIGURERS' in inherited_settings:
        new_settings['ADMIN_CONFIGURERS'] = inherited_settings[
            'ADMIN_CONFIGURERS']
        new_settings['ADMIN_CONFIGURERS'] += ((
            'new_infoshina_theme',
            _(u'New infoshina theme'),
            'new_infoshina_theme.configurer.InfoshinaThemeConfig'
        ),)

    default_buy_button_template = \
        '''<p><button class="btn button_add_cart">Купить</button></p>'''
    default_product_not_available_tamplate =\
        '''<p><span class="btn disabled"
        style="font-size: 11px;">Нет в наличии</span></p>'''
    buy_on_credit_button_template =\
        '''<button id="button_add_cart_credit"
        class="btn credit">Купить в кредит</button>'''
    credit_processing_button_template =\
        '''<p>Для перехода на страницу банка и дальнейшего оформления
        кредита нажмите на кнопку</p><button class="credit_btn btn">
        Оформление кредита</<button>'''
    all_variants_button_template =\
        '''<a href="{{ link }}" class="btn ajax-load"
        data-container="#variants-{{ pk }}">
        <p>Все варианты</p></a>'''
    esender_template = '''
<form action="/subscribe-abonent/" id="subscribe_abonent_{{ portlet_id }}" method="POST">
<p>{% csrf_token %}</p>

<p>{{ form.media }}</p>

<p><input id="id_email" maxlength="200" name="email" placeholder="Почта" type="text" /></p>

<p><input id="id_name" maxlength="200" name="name" placeholder="Имя" type="text" />&nbsp;</p>

<p>&nbsp;<input id="subscribe_abonent_submit_{{ portlet_id }}" type="submit" value="Подписаться" /></p>
</form>
'''
    one_click_order_button_template = '''
    {% if not is_set %}
     <a id="buy_one_click" type="button"
     href="{% url make_one_click_order product_id %}"
     class="btn btn-primary modal-link">
        Купить в один клик
     </a>
     <script type="text/javascript">
     $('#buy_one_click').click(function(){
        quan = $('input[name="quantity"]')[0].value
        this.href += '?amount=' + quan
     });
     </script>
    {% else %}
    <a id="buy_one_click" type="button"
    href="{% url make_one_click_set_order %}?{{ params }}"
    class="btn btn-primary modal-link">
        Купить в один клик
    </a>
    {% endif %}
    '''

    if 'INFOSHINA_THEME_BUY_BUTTON_TEMPLATE' not in inherited_settings:
        new_settings['INFOSHINA_THEME_BUY_BUTTON_TEMPLATE'] = \
            default_buy_button_template
    if 'INFOSHINA_THEME_BUY_BUTTON_ON_CATEGORY_TEMPLATE' not in inherited_settings:
        new_settings['INFOSHINA_THEME_BUY_BUTTON_ON_CATEGORY_TEMPLATE'] = \
            default_buy_button_template
    if 'INFOSHINA_THEME_BUY_BUTTON_ON_MODEL_TEMPLATE' not in inherited_settings:
        new_settings['INFOSHINA_THEME_BUY_BUTTON_ON_MODEL_TEMPLATE'] = \
            default_buy_button_template
    if 'INFOSHINA_THEME_PRODUCT_NOT_AVAILABLE_TEMPLATE'\
            not in inherited_settings:
        new_settings['INFOSHINA_THEME_PRODUCT_NOT_AVAILABLE_TEMPLATE'] = \
            default_product_not_available_tamplate
    if 'INFOSHINA_THEME_BUY_ON_CREDIT_BUTTON_TEMPLATE'\
            not in inherited_settings:
        new_settings['INFOSHINA_THEME_BUY_ON_CREDIT_BUTTON_TEMPLATE'] = \
            buy_on_credit_button_template
    if 'INFOSHINA_THEME_CREDIT_PROCESSING_BUTTON_TEMPLATE'\
            not in inherited_settings:
        new_settings['INFOSHINA_THEME_CREDIT_PROCESSING_BUTTON_TEMPLATE'] = \
            credit_processing_button_template
    if 'INFOSHINA_THEME_SHOW_CREDIT_BUTTONS' not in inherited_settings:
        new_settings['INFOSHINA_THEME_SHOW_CREDIT_BUTTONS'] = False
    if 'INFOSHINA_THEME_ALL_VARIANTS_BUTTON_TEMPLATE'\
            not in inherited_settings:
        new_settings['INFOSHINA_THEME_ALL_VARIANTS_BUTTON_TEMPLATE'] =\
            all_variants_button_template
    if 'INFOSHINA_THEME_ESENDER_TEMPLATE' not in inherited_settings:
        new_settings['INFOSHINA_THEME_ESENDER_TEMPLATE'] = esender_template
    if 'INFOSHINA_THEME_ESENDER_PRODUCT_TEMPLATE' not in inherited_settings:
        new_settings['INFOSHINA_THEME_ESENDER_PRODUCT_TEMPLATE'] =\
            esender_template
    if 'INFOSHINA_THEME_ONE_CLICK_ORDER_BUTTON_TEMPLATE'\
            not in inherited_settings:
        new_settings['INFOSHINA_THEME_ONE_CLICK_ORDER_BUTTON_TEMPLATE'] =\
            one_click_order_button_template

    if 'EXTRA_JS' in inherited_settings:
        new_settings['EXTRA_JS'] = inherited_settings['EXTRA_JS'] +\
            ['js/get_products_statuses.js']
    new_settings['MIDDLEWARE_CLASSES'] =\
        inherited_settings['MIDDLEWARE_CLASSES'] + (
            'new_infoshina_theme.middleware.InfoshinaRedirectMiddleware',
            'new_infoshina_theme.middleware.Redirect404Middleware'
            )
    return new_settings


def update_urls(urls_globals):
    '''Update url routes.
    '''
    # from django.conf import settings
    from django.conf.urls import patterns, include, url
    import reviews.utils
    from new_infoshina_theme.utils import get_mark, get_average_for_instance
    reviews.utils.get_mark = get_mark
    reviews.utils.get_average_for_instance = get_average_for_instance
    # overwriting notebook
    from new_infoshina_theme.views import (
        add_product_view,
        ajax_notebook_render,
        remove_product_view,
        notebook_view,
        remove_all_products_view,
        notebook_remove_product_ajax,
        remove_all_products_ajax,
        get_products_ajax
    )
    import lfs.notebook.views
    lfs.notebook.views.add_product_view = add_product_view
    lfs.notebook.views.ajax_notebook_render = ajax_notebook_render
    lfs.notebook.views.remove_product_view = remove_product_view
    lfs.notebook.views.notebook_view = notebook_view
    lfs.notebook.views.remove_all_products_view = remove_all_products_view
    lfs.notebook.views.notebook_remove_product_ajax =\
        notebook_remove_product_ajax
    lfs.notebook.views.remove_all_products_ajax = notebook_remove_product_ajax
    lfs.notebook.views.get_products_ajax = get_products_ajax
    urlpatterns = patterns(
        '',
        url(r'', include('new_infoshina_theme.urls')),
    )
    urlpatterns += urls_globals['urlpatterns']
    return {
        'urlpatterns': urlpatterns,
    }


def install(project_dir=''):
    import subprocess,os
    CMDS = [
        'python %s collectstatic --noinput' % os.path.join(project_dir, '../manage.py'),
        'python %s compress' % os.path.join(project_dir, '../manage.py'),
    ]
    for c in CMDS:
        subprocess.call(c, shell=True)


def upgrade(from_version, to_version, project_dir=''):
    import subprocess,os
    CMDS = [
        'python %s collectstatic --noinput' % os.path.join(project_dir, '../manage.py'),
        'python %s compress' % os.path.join(project_dir, '../manage.py'),
    ]
    for c in CMDS:
        subprocess.call(c, shell=True)
    #print 'Update from', from_version, ' to ', to_version
    pass
