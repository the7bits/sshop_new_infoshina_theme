# -*- coding: utf-8 -*-
from django.template import Library
from lfs.filters.models import FilterOption
from django.utils.safestring import mark_safe
from lxml.html import document_fromstring, tostring
from urllib import urlencode, unquote
from django.template.defaultfilters import stringfilter
# from urlparse import parse_qs
# from sbits_pages.models import SCMS_News
from lfs.catalog.models import PropertyValueIcon, Product, ProductPropertyValue
from lfs.catalog.templatetags.properties_tags import _get_property_value
from lfs.catalog.models import Product, ProductPropertyValue
from django.conf import settings
from lfs.core.utils import render_meta_info
from django.core.urlresolvers import reverse
from lfs.catalog.models import Property

register = Library()


@register.filter(name='correct_variants')
def correct_variants(product):
    if product.parent is None:
        variants = product.get_variants()
    else:
        variants = product.parent.get_variants()
    variants = variants.exclude(id=product.id)
    return variants


@register.simple_tag
def correct_variants_count(product):
    if product.parent is None:
            variants = product.get_variants()
    else:
        variants = product.parent.get_variants()
    variants = variants.exclude(id=product.id)
    return variants.count()


@register.filter(name='correct_variants_property')
def correct_variants_property(product_variants):
    not_empty_variants = []
    for variant in product_variants:
        product_property_values = variant.get_property_values()
        list_property = [
            x['property__identificator']
            for x in product_property_values.values('property__identificator')]
        if 'shiny' in variant.get_category().slug:
            if "diametr" in list_property:
                not_empty_variants.append(variant)
        elif 'diski' in variant.get_category().slug:
            if "diametr" in list_property:
                not_empty_variants.append(variant)
    return not_empty_variants


@register.inclusion_tag(
    'lfs/catalog/products/value_icons.html', takes_context=True)
def property_value_icon(context, product, identificator):
    p_icons = PropertyValueIcon.objects.filter(
        products=product, properties__identificator=identificator)
    if not p_icons and int(product.sub_type) == 1:
        variants = product.variants.filter(status__is_visible=True)
        if variants:
            p_icons = PropertyValueIcon.objects.filter(
                products=variants[0], properties__identificator=identificator)
    return {
        'icons': [p_icons[0]] if p_icons else [],
    }


@register.simple_tag(takes_context=True)
def property_for_product_custom(context, product, property_identifier):
    value = ''
    try:
        try:
            p_value = ProductPropertyValue.objects.get(
                product=product, property__identificator=property_identifier)
            value = p_value.value
        except ProductPropertyValue.DoesNotExist:
            value = ''
        if not value and int(product.sub_type) == 1:
            variants = product.variants.filter(status__is_visible=True)
            if variants:
                p_value = ProductPropertyValue.objects.get(
                    product=variants[0],
                    property__identificator=property_identifier)
                value = p_value.value
    except ProductPropertyValue.DoesNotExist:
        value = ''
    return value


@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_title(context, block_super):
    title_min_max = u'Купить автомобильные {type} цена от {min}\
 до {max} гривен в интернет-магазине Infoshina'
    title_min = u'Купить автомобильные {type} цена от {min}\
 гривен в интернет-магазине Infoshina'
    title_max = u'Купить автомобильные {type} цена до {max}\
 гривен в интернет-магазине Infoshina'
    title = None
    _type = u'шины'
    if 'diski' == context['category_slug']:
        _type = u'диски'
    elif 'shiny' == context['category_slug']:
        _type = u'шины'

    get = context['request'].GET.copy()
    get.pop('start', None)
    if len(get) == 1 and 'price' in get:
        for filter_name, value in get.items():
            if 'price' == filter_name:
                _min, _max = value.split('..')
                if _min and _max:
                    title = title_min_max.format(
                        min=_min,
                        max=_max,
                        type=_type)
                elif _min:
                    title = title_min.format(min=_min, type=_type)
                elif _max:
                    title = title_max.format(max=_max, type=_type)
    return {
        'custom_seo': title or block_super
    }


@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_keywords(context, block_super):
    keywords_min_max = u'диски цена от {min}\
 до {max} гривен, купить диски цена от {min} гр, диски цена до {max} гр цена,\
 интернет-магазин, infoshina.com.ua'
    keywords_min = u'диски цена от {min}\
 гривен, купить диски цена от {min} гр, интернет-магазин, infoshina.com.ua'
    keywords_max = u'диски цена до {max}\
 гривен, диски цена до {max} гр цена, интернет-магазин, infoshina.com.ua'
    keywords_min_max_shiny = u'шины цена от {min}\
 до {max} гривен, купить резину цена от {min} гр, \
 покрышки цена до {max} гр цена,\
 интернет-магазин, infoshina.com.ua'
    keywords_min_shiny = u'шины цена от {min}\
, купить резину цена от {min}, интернет-магазин, infoshina.com.ua'
    keywords_max_shiny = u'шины цена до {max}\
, покрышки цена до {max} цена, интернет-магазин, infoshina.com.ua'
    keywords = None

    get = context['request'].GET.copy()
    get.pop('start', None)
    if len(get) == 1 and 'price' in get:
        for filter_name, value in get.items():
            if 'diski' == context['category_slug']:
                if 'price' == filter_name:
                    _min, _max = value.split('..')
                    if _min and _max:
                        keywords = keywords_min_max.format(min=_min, max=_max)
                    elif _min:
                        keywords = keywords_min.format(min=_min)
                    elif _max:
                        keywords = keywords_max.format(max=_max)
            elif 'shiny' == context['category_slug']:
                if 'price' == filter_name:
                    _min, _max = value.split('..')
                    if _min and _max:
                        keywords = keywords_min_max_shiny.format(
                            min=_min, max=_max)
                    elif _min:
                        keywords = keywords_min_shiny.format(min=_min)
                    elif _max:
                        keywords = keywords_max_shiny.format(max=_max)
    return {
        'custom_seo': keywords or block_super
    }


@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_description(context, block_super):
    description_min_max = u'Хотите купить {type} ценой от {min}\
 до {max} гр с доставкой по Украине? Заходите к нам в интернет-магазин\
 - infoshina.com.ua'
    description_min = u'Хотите купить {type} ценой от {min}\
 гр с доставкой по Украине? Заходите к нам в интернет-магазин - \
 infoshina.com.ua'
    description_max = u'Хотите купить {type} ценой до {max}\
 гр с доставкой по Украине? Заходите к нам в интернет-магазин - \
 infoshina.com.ua'
    description = None
    _type = u'резину автомобильную'
    if 'diski' in context['category_slug']:
        _type = u'диски автомобильные'
    elif context['category_slug'] == 'shiny':
        _type = u'резину автомобильную'
    get = context['request'].GET.copy()
    get.pop('start', None)
    if len(get) == 1:
        for filter_name, value in get.items():
            if 'price' == filter_name:
                _min, _max = value.split('..')
                if _min and _max:
                    description = description_min_max.format(
                        min=_min,
                        max=_max,
                        type=_type)
                elif _min:
                    description = description_min.format(min=_min, type=_type)
                elif _max:
                    description = description_max.format(max=_max, type=_type)
    return {
        'custom_seo': description or block_super
    }


def get_title_by_identificators(filter__identificator, identificator):
    values = FilterOption.objects.filter(
        filter__identificator=filter__identificator,
        identificator=identificator
    )
    if values:
        return values[0].title
    else:
        return ''


@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_h1(context, category):
    h1_min_max = u'{type} цена от {min} до {max} гривен'
    h1_min = u'{type} цена от {min} гривен'
    h1_max = u'{type} цена до {max} гривен'
    h1_width = u'{type} ширина {value}'
    h1_diam = u'{type} {value}'
    h1_disk_pcd = u'Диски PCD {value}'
    h1_disk_dia = u'Диски DIA {value}'
    h1_disk_et = u'Диски ET {value}'
    h1_disk_type = u'Диски {value}'
    h1_tire_prof = 'Шины профиль {value}'
    h1_tire_season = u'Шины {value}'
    h1_car_type_1 = u'Шины для легковых автомобилей'
    h1_car_type_2 = u'Шины для внедорожников'
    h1_car_type_3 = u'Шины для легкогрузовых автомобилей'
    h1_car_type_4 = u'Шины для грузовых автомобилей'
    h1_brand = u'{type} {value}'
    h1_not_ship = u'Не шипованные зимние шины'
    h1_pod_ship = u'Зимние шины под шипование'
    h1_ship = u'Шипованные зимние шины'
    h1 = None
    _type = category.name
    if category.slug == 'diski':
        _type = u'Диски'
    elif category.slug == 'shiny':
        _type = u'Шины'
    get = context['request'].GET.copy()
    get.pop('start', None)
    if len(get) == 1\
            and len(get.values()[0].split(',')) == 1:
        for filter_name, value in get.items():
            if 'price' != filter_name and 'tip_avto' != filter_name:
                value = get_title_by_identificators(
                    filter_name,
                    value
                )
            if 'price' == filter_name:
                _min, _max = value.split('..')
                if _min and _max:
                    h1 = h1_min_max.format(
                        min=_min,
                        max=_max,
                        type=_type)
                elif _min:
                    h1 = h1_min.format(min=_min, type=_type)
                elif _max:
                    h1 = h1_max.format(max=_max, type=_type)
            elif 'wdiski' == filter_name or 'wshiny' == filter_name:
                h1 = h1_width.format(value=value, type=_type)
            elif 'rdiski' == filter_name or 'rshiny' == filter_name:
                h1 = h1_diam.format(value=value, type=_type)
            elif 'pcd' == filter_name:
                h1 = h1_disk_pcd.format(value=value)
            elif 'et' == filter_name:
                h1 = h1_disk_et.format(value=value)
            elif 'dia' == filter_name:
                h1 = h1_disk_dia.format(value=value)
            elif 'tip_diska' == filter_name:
                h1 = h1_disk_type.format(value=value)
            elif 'hshiny' == filter_name:
                h1 = h1_tire_prof.format(value=value)
            elif 'sezon' == filter_name:
                h1 = h1_tire_season.format(value=value)
            elif 'brand' == filter_name:
                h1 = h1_brand.format(value=value, type=_type)
            elif 'tip_avto' == filter_name:
                if value == 'legkovoj':
                    h1 = h1_car_type_1
                elif value == 'vnedorozhnik':
                    h1 = h1_car_type_2
                elif value == 'legko-gruzovoj':
                    h1 = h1_car_type_3
                elif value == 'gruzovoj':
                    h1 = h1_car_type_4

    if len(get) == 2:
        query = get
        if 'sezon' in query and 'ship' in query:
            if query['sezon'] == 'zimnie' and query['ship'] == 'ship':
                h1 = h1_ship
            elif query['sezon'] == 'zimnie' and query['ship'] == 'bez-shipa':
                h1 = h1_not_ship
            elif query['sezon'] == 'zimnie' and query['ship'] == 'pod-ship':
                h1 = h1_pod_ship
    return {
        'custom_seo': h1 or category.name
    }


@register.inclusion_tag(
    'custom_seo/custom_breadcrumbs.html', takes_context=True)
def custom_breadcrumbs(context, category):
    breadcrumbs_min_max = u'{type} стоимостью от {min} до {max} гривен'
    breadcrumbs_min = u'{type} цена от {min} гривен'
    breadcrumbs_max = u'{type} цена до {max} гривен'
    breadcrumbs_width = u'{type} ширина {value}'
    breadcrumbs_diam = u'{type} {value}'
    breadcrumbs_disk_pcd = u'Диски PCD {value}'
    breadcrumbs_disk_dia = u'Диски DIA {value}'
    breadcrumbs_disk_et = u'Диски ET {value}'
    breadcrumbs_disk_type = u'Диски {value}'
    breadcrumbs_tire_prof = 'Шины профиль {value}'
    breadcrumbs_tire_season = u'Шины {value}'
    breadcrumbs_car_type_1 = u'Шины для легковых автомобилей'
    breadcrumbs_car_type_2 = u'Шины для внедорожников'
    breadcrumbs_car_type_3 = u'Шины для легкогрузовых автомобилей'
    breadcrumbs_car_type_4 = u'Шины для грузовых автомобилей'
    breadcrumbs_brand = u'{type} производства {value}'
    breadcrumbs_not_ship = u'Не шипованные зимние шины'
    breadcrumbs_pod_ship = u'Зимние шины под шипование'
    breadcrumbs_ship = u'Шипованные зимние шины'
    breadcrumbs = None
    _type = ''
    if category.slug == 'diski':
        _type = u'Диски'
    elif category.slug == 'shiny':
        _type = u'Шины'
    get = context['request'].GET.copy()
    get.pop('start', None)
    if len(get) == 1\
            and len(get.values()[0].split(',')) == 1:
        for filter_name, value in get.items():
            if 'price' != filter_name and 'tip_avto' != filter_name:
                value = get_title_by_identificators(
                    filter_name,
                    value
                )
            if 'price' == filter_name:
                _min, _max = value.split('..')
                if _min and _max:
                    breadcrumbs = breadcrumbs_min_max.format(
                        min=_min,
                        max=_max,
                        type=_type)
                elif _min:
                    breadcrumbs = breadcrumbs_min.format(min=_min, type=_type)
                elif _max:
                    breadcrumbs = breadcrumbs_max.format(max=_max, type=_type)
            elif 'wdiski' == filter_name or 'wshiny' == filter_name:
                breadcrumbs = breadcrumbs_width.format(value=value, type=_type)
            elif 'rdiski' == filter_name or 'rshiny' == filter_name:
                breadcrumbs = breadcrumbs_diam.format(value=value, type=_type)
            elif 'pcd' == filter_name:
                breadcrumbs = breadcrumbs_disk_pcd.format(value=value)
            elif 'et' == filter_name:
                breadcrumbs = breadcrumbs_disk_et.format(value=value)
            elif 'dia' == filter_name:
                breadcrumbs = breadcrumbs_disk_dia.format(value=value)
            elif 'tip_diska' == filter_name:
                breadcrumbs = breadcrumbs_disk_type.format(value=value)
            elif 'hshiny' == filter_name:
                breadcrumbs = breadcrumbs_tire_prof.format(value=value)
            elif 'sezon' == filter_name:
                breadcrumbs = breadcrumbs_tire_season.format(value=value)
            elif 'brand' == filter_name:
                breadcrumbs = breadcrumbs_brand.format(value=value, type=_type)
            elif 'tip_avto' == filter_name:
                if value == 'legkovoj':
                    breadcrumbs = breadcrumbs_car_type_1
                elif value == 'vnedorozhnik':
                    breadcrumbs = breadcrumbs_car_type_2
                elif value == 'legko-gruzovoj':
                    breadcrumbs = breadcrumbs_car_type_3
                elif value == 'gruzovoj':
                    breadcrumbs = breadcrumbs_car_type_4

    if len(get) == 2:
        query = get
        if 'sezon' in query and 'ship' in query:
            if query['sezon'] == 'zimnie' and query['ship'] == 'ship':
                breadcrumbs = breadcrumbs_ship
            elif query['sezon'] == 'zimnie' and query['ship'] == 'bez-shipa':
                breadcrumbs = breadcrumbs_not_ship
            elif query['sezon'] == 'zimnie' and query['ship'] == 'pod-ship':
                breadcrumbs = breadcrumbs_pod_ship
    return {
        'custom_breadcrumbs': breadcrumbs or ''
    }


@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_prev_and_next(context, block_super):
    tag_next_prev = (
        u'<link rel="prev" href="http://{get_host_}{get_}?start={_prev}">\
 <link rel="next" href="http://{get_host_}{get_}?start={_next}">')
    tag_next_prev_second = (
        u'<link rel="prev" href="http://{get_host_}{get_}">\
 <link rel="next" href="http://{get_host_}{get_}?start={_next}">')
    tag_next = (
        u'<link rel="next" href="http://{get_host_}{get_}?start={_next}">')
    tag_prev = (
        u'<link rel="prev" href="http://{get_host_}{get_}?start={_prev}">')
    tag_prev_second = (u'<link rel="prev" href="http://{get_host_}{get_}">')
    next_prev = None
    request = context['request']
    get = context['request'].GET.copy()
    get_host = request.get_host()
    other_get = dict()
    if '?' in request.get_full_path():
        for param in request.get_full_path().split('?')[1].split('&'):
            key, value = param.split('=')
            other_get[key] = value
    other_get.pop('start', None)
    if other_get:
        tag_next_prev = (
            u'<link rel="prev" href="http://{get_host_}{get_}?\
start={_prev}&{other_get}">\
 <link rel="next" href="{get_}?start={_next}&{other_get}">')
        tag_next_prev_second = (
            u'<link rel="prev" href="http://{get_host_}{get_}">\
 <link rel="next" href="http://{get_host_}{get_}?start={_next}">')
        tag_next = (u'<link rel="next" href="http://{get_host_}{get_}?\
start={_next}&{other_get}">')
        tag_prev = (u'<link rel="prev" href="http://{get_host_}{get_}?\
start={_prev}&{other_get}">')
        tag_prev_second = (
            u'<link rel="prev" href="http://{get_host_}{get_}">')
        other_get = unquote(urlencode(other_get)).decode('utf8')
        get_host = request.get_host()
    _get = request.path
    category_inline_html = document_fromstring(context['category_inline'])
    if category_inline_html.cssselect('.last-page'):
        last = int(
            tostring(
                category_inline_html.cssselect('.last-page a')[0]
            ).split('start=')[1].replace('\'', '"').split('"')[0].split('&')[0]
        )
    elif category_inline_html.cssselect(
            '.pagination li:last-child span.current'):
        last = int(
            category_inline_html.cssselect(
                '.pagination li:last-child span.current')[0].text)
    else:
        numbers = []
        for link in category_inline_html.cssselect('.pagination li a'):
            if 'start' in tostring(link):
                numbers.append(
                    int(
                        tostring(link).split(
                            'start=')[1].replace(
                                '\'', '"').split('"')[0].split('&')[0])
                )
        if numbers:
            last = max(numbers)
        else:
            last = 1
    for start_items, value in get.items():
        if 'start' == start_items:
            next = int(value) + 1
            prev = int(value) - 1
            if int(value) == 1:
                next_prev = mark_safe(tag_next.format(
                    get_=_get,
                    get_host_=get_host,
                    _next=next,
                    _prev=prev,
                    other_get=other_get
                ))
            elif int(value) == 2 and int(value) == last:
                next_prev = mark_safe(tag_prev_second.format(
                    get_=_get,
                    get_host_=get_host,
                    _next=next,
                    _prev=prev,
                    other_get=other_get
                ))
            elif int(value) == 2:
                next_prev = mark_safe(tag_next_prev_second.format(
                    get_=_get,
                    get_host_=get_host,
                    _next=next,
                    _prev=prev,
                    other_get=other_get
                ))
            elif int(value) == last:
                next_prev = mark_safe(tag_prev.format(
                    get_=_get,
                    get_host_=get_host,
                    _next=next,
                    _prev=prev,
                    other_get=other_get
                ))
            else:
                next_prev = mark_safe(tag_next_prev.format(
                    get_=_get,
                    get_host_=get_host,
                    _next=next,
                    _prev=prev,
                    other_get=other_get
                ))

    if 'start' not in get.keys() and last != 1:
        next_prev = mark_safe(tag_next.format(
            get_=_get,
            get_host_=get_host,
            _next=2,
            _prev=0,
            other_get=other_get
        ))
    return {
        'custom_seo': next_prev or block_super
    }


@register.filter(name='sorted_by_manufacturer')
def sorted_by_manufacturer(products):
    from lfs.catalog.models import Property
    model = Property.objects.get(identificator=u'model')
    sorted_products = []
    for p in products:
        sorted_products.append(
            [p.get_variants(
            )[0].manufacturer.name + ' ' + p.get_variants()[0].get_property_value(
                model).value, p])
    sorted_products = sorted(sorted_products)
    return sorted_products


@register.inclusion_tag('custom_seo/custom_seo.html', takes_context=True)
def custom_prev_and_next_product_list(context, block_super):
    tag_next_prev = (
        u'<link rel="prev" href="http://{get_host_}{get_}?start={_prev}">\
        <link rel="next" href="http://{get_host_}{get_}?start={_next}">')
    tag_next_prev_second = (
        u'<link rel="prev" href="http://{get_host_}{get_}">\
        <link rel="next" href="http://{get_host_}{get_}?start={_next}">')
    tag_next = (
        u'<link rel="next" href="http://{get_host_}{get_}?start={_next}">')
    tag_prev = (
        u'<link rel="prev" href="http://{get_host_}{get_}?start={_prev}">')
    next_prev = None
    request = context['request']
    get = context['request'].GET.copy()
    other_get = dict()
    # import pdb
    # pdb.set_trace()
    if '?' in request.get_full_path():
        for param in request.get_full_path().split('?')[1].split('&'):
            key, value = param.split('=')
            other_get[key] = value
    other_get.pop('start', None)
    if other_get:
        tag_next_prev = (u'<link rel="prev" href="http://{get_host_}{get_}?\
            start={_prev}&{other_get}"><link rel="next" href="\
            http://{get_host_}{get_}?start={_next}&{other_get}">')
        tag_next = (u'<link rel="next" href="http://{get_host_}{get_}?\
            start={_next}&{other_get}">')
        tag_prev = (u'<link rel="prev" href="http://{get_host_}{get_}?\
            start={_prev}&{other_get}">')
        other_get = unquote(urlencode(other_get)).decode('utf8')
    _get = request.path
    get_host = request.get_host()
    if 'pagination' not in context:
        return {'custom_seo': block_super}
    last = context['pagination']['total_pages']
    # for start_items, value in get.items():
    #     if 'start' == start_items:
    next = int(context['pagination']['current_page']) + 1
    prev = int(context['pagination']['current_page']) - 1
    if int(context['pagination']['current_page']) == 1:
        next_prev = mark_safe(tag_next.format(
            get_=_get,
            get_host_=get_host,
            _next=next,
            _prev=prev,
            other_get=other_get
        ))
        if last == 1:
            next_prev = None
    elif int(context['pagination']['current_page']) == 2:
        next_prev = mark_safe(
            tag_next_prev_second.format(
                get_=_get,
                get_host_=get_host,
                _next=next,
                _prev=prev,
                other_get=other_get
            )
        )
    elif int(context['pagination']['current_page']) == last:
        next_prev = mark_safe(tag_prev.format(
            get_=_get,
            get_host_=get_host,
            _next=next,
            _prev=prev,
            other_get=other_get
        ))
    else:
        next_prev = mark_safe(tag_next_prev.format(
            get_=_get,
            get_host_=get_host,
            _next=next,
            _prev=prev,
            other_get=other_get
        ))

    # if 'start' not in get.keys() and last != 1:
    #     next_prev = mark_safe(tag_next.format(
    #     get_=_get, _next=2, _prev=0, other_get=other_get))
    return {
        'custom_seo': next_prev or block_super
    }


def _get_property_identifier(product, filter_identifier):
    value = ''
    try:
        p_property = product.filteroption_set.filter(
            filter__identificator=filter_identifier)
        if p_property:
            p_property = p_property[0]
        # value = p_value.identifier
            value = p_property.identificator
        else:
            value = ''
    except ProductPropertyValue.DoesNotExist:
        value = ''
    return value


@register.simple_tag(takes_context=True)
def property_for_product_identifier(context, product, property_identifier):
    return _get_property_identifier(product, property_identifier)


@register.simple_tag(takes_context=True)
def complete_property_for_product(context, product, property_identifier):
    try:
        p = Property.objects.get(identificator=property_identifier)
        val = product.get_property_value(p).value
    except:
        return ''
    if p.name != '' and val != '':
        return '<li>%s <span>%s</span></li>' % (p.name, val)
    else:
        return ''


@register.filter(name='sorted_by_diametr')
def sorted_by_diametr(products):
    ids = [p.id for p in products]
    category = products[0].get_category().slug
    properties = [u'diametr']
    if category == u'shiny':
        properties.append(u'shirina-shinyi')
    if category == u'diski':
        properties.append(u'shirina-diska')
    sorted_products = Product.objects.filter(
        id__in=ids,
        property_values__property__identificator__in=properties,
        categories__slug__in=['shiny']).distinct()
    values = Product.objects.filter(
        id__in=ids).values(
            'property_values__value',
            'property_values__property__identificator',
            'id'
        )
    products_values = {}
    for v in values:
        v['property_values__property__identificator'] =\
            v['property_values__property__identificator'].replace('-', '_')
        if v['id'] in products_values:
            products_values[v['id']][
                v['property_values__property__identificator']] =\
                    v['property_values__value']
        else:
            products_values[v['id']] = {
                v['property_values__property__identificator']: v[
                    'property_values__value']
            }
    if category == u'shiny':
        shirina = u'shirina_shinyi'
    if category == u'diski':
        shirina = u'shirina_diska'
    sorted_products = []
    for p in products:
        if p.id not in products_values:
            continue
        p.properties = products_values[p.id]
        if 'diametr' in p.properties and p.properties['diametr']\
                and shirina in p.properties and p.properties[shirina]:
            sorted_products.append(
                [
                    (
                        p.properties['diametr'],
                        p.properties[shirina]
                    ),
                    p
                ]
            )
    sorted_products = sorted(sorted_products)
    last_r = None
    for tup in sorted_products:
        if tup[0][0] != last_r:
            tup[0] += (True, )
            last_r = tup[0][0]
        else:
            tup[0] += (False, )
    return sorted_products


@register.filter(name='split')
def split(str, splitter):
    return str.split(splitter)


@register.assignment_tag(takes_context=True)
def get_filteroption_name(context, request_get, filter_identifier, category):
    if request_get and context and filter_identifier and category:
        if filter_identifier in request_get.keys() and\
                len(request_get[filter_identifier].split(',')) == 1:
            f = FilterOption.objects.get(
                filter__identificator=filter_identifier,
                filter__category=category,
                identificator=request_get[filter_identifier])
            return f.title
    return ''


@register.filter
@stringfilter
def capitalize(value):
    return " ".join(value.strip().split()).capitalize()


@register.assignment_tag(takes_context=False)
def splited_property_for_product(product, property_identifier):
    speed = _get_property_value(product, "indeks-skorosti")
    load = _get_property_value(product, "indeks-nagruzki")
    value = ""

    if property_identifier == "index":
        value = load.split("-")[0].strip() + speed.split("-")[0].strip()

    elif property_identifier == "values":
        if '-' in load and load.split('-')[-1].strip().startswith(u'до'):
            value = load.split('-')[-1].strip()[2:] + ", " +\
                speed.split('-')[-1].strip()
        elif '-' in load:
            value = load.split('-')[-1].strip() + ", " +\
                speed.split('-')[-1].strip()
        else:
            value = speed.split('-')[-1].strip()

    return value


@register.filter()
def replace_amp(args_str):
    args_str = args_str.lstrip('&')
    if args_str:
        args_str = '?' + args_str
    return args_str


DEFAULT_WINDOW = getattr(settings, 'PAGINATION_DEFAULT_WINDOW', 4)


@register.inclusion_tag('pagination/pagination.html', takes_context=True)
def paginate(context, window=DEFAULT_WINDOW, hashtag=''):
    """
    Renders the ``pagination/pagination.html`` template, resulting in a
    Digg-like display of the available pages, given the current page.  If there
    are too many pages to be displayed before and after the current page, then
    elipses will be used to indicate the undisplayed gap between page numbers.

    Requires one argument, ``context``, which should be a dictionary-like data
    structure and must contain the following keys:

    ``paginator``
        A ``Paginator`` or ``QuerySetPaginator`` object.

    ``page_obj``
        This should be the result of calling the page method on the
        aforementioned ``Paginator`` or ``QuerySetPaginator`` object, given
        the current page.

    This same ``context`` dictionary-like data structure may also include:

    ``getvars``
        A dictionary of all of the **GET** parameters in the current request.
        This is useful to maintain certain types of state, even when requesting
        a different page.
        """
    try:
        paginator = context['paginator']
        page_obj = context['page_obj']
        page_range = paginator.page_range
        # Calculate the record range in the current page for display.
        records = {'first': 1 + (page_obj.number - 1) * paginator.per_page}
        records['last'] = records['first'] + paginator.per_page - 1
        if records['last'] + paginator.orphans >= paginator.count:
            records['last'] = paginator.count
        # First and last are simply the first *n* pages and the last *n* pages,
        # where *n* is the current window size.
        first = set(page_range[:window])
        last = set(page_range[-window:])
        # Now we look around our current page, making sure that we don't wrap
        # around.
        current_start = page_obj.number - 1 - window
        if current_start < 0:
            current_start = 0
        current_end = page_obj.number - 1 + window
        if current_end < 0:
            current_end = 0
        current = set(page_range[current_start:current_end])
        pages = []
        # If there's no overlap between the first set of pages and the current
        # set of pages, then there's a possible need for elusion.
        if len(first.intersection(current)) == 0:
            first_list = list(first)
            first_list.sort()
            second_list = list(current)
            second_list.sort()
            pages.extend(first_list)
            diff = second_list[0] - first_list[-1]
            # If there is a gap of two, between the last page of the first
            # set and the first page of the current set, then we're missing a
            # page.
            if diff == 2:
                pages.append(second_list[0] - 1)
            # If the difference is just one, then there's nothing to be done,
            # as the pages need no elusion and are correct.
            elif diff == 1:
                pass
            # Otherwise, there's a bigger gap which needs to be signaled for
            # elusion, by pushing a None value to the page list.
            else:
                pages.append(None)
            pages.extend(second_list)
        else:
            unioned = list(first.union(current))
            unioned.sort()
            pages.extend(unioned)
        # If there's no overlap between the current set of pages and the last
        # set of pages, then there's a possible need for elusion.
        if len(current.intersection(last)) == 0:
            second_list = list(last)
            second_list.sort()
            diff = second_list[0] - pages[-1]
            # If there is a gap of two, between the last page of the current
            # set and the first page of the last set, then we're missing a
            # page.
            if diff == 2:
                pages.append(second_list[0] - 1)
            # If the difference is just one, then there's nothing to be done,
            # as the pages need no elusion and are correct.
            elif diff == 1:
                pass
            # Otherwise, there's a bigger gap which needs to be signaled for
            # elusion, by pushing a None value to the page list.
            else:
                pages.append(None)
            pages.extend(second_list)
        else:
            differenced = list(last.difference(current))
            differenced.sort()
            pages.extend(differenced)
        to_return = {
            'MEDIA_URL': settings.MEDIA_URL,
            'pages': pages,
            'records': records,
            'page_obj': page_obj,
            'paginator': paginator,
            'hashtag': hashtag,
            'is_paginated': paginator.count > paginator.per_page,
        }
        if 'request' in context:
            getvars = context['request'].GET.copy()
            if 'page' in getvars:
                del getvars['page']
            if len(getvars.keys()) > 0:
                to_return['getvars'] = "&%s" % getvars.urlencode()
            else:
                to_return['getvars'] = ''
            to_return['request'] = context['request']
        return to_return
    except KeyError, AttributeError:
        return {}


@register.assignment_tag(takes_context=True)
def link_to_mobile(context, slug=""):
    domain = getattr(settings, 'PCART_API_DOMAIN', False)
    if not domain:
        return None
    path = filter(None, context['request'].path.split('/'))
    sortings = context['request'].GET
    mobile_domain = domain.rstrip('/')
    if path:
        path_to_mobile = [mobile_domain, slug] + [path[0]]
    else:
        path_to_mobile = [mobile_domain, slug] + path
    final_path = '/'.join(filter(None, path_to_mobile)) + "/"
    if sortings:
        result = "?".join([final_path, urlencode(sortings)])
        return result
    return final_path


@register.simple_tag(takes_context=True)
def get_recent_products_ids(context, slugs):
    ids = [
        p['id'] for p in Product.objects.filter(
            slug__in=slugs).values('id')]
    return ids


@register.simple_tag
def buy_button_html(param):
    template = None
    if param == 'product_page':
        template = settings.INFOSHINA_THEME_BUY_BUTTON_TEMPLATE
    elif param == 'category_page':
        template = settings.INFOSHINA_THEME_BUY_BUTTON_ON_CATEGORY_TEMPLATE
    else:
        template = settings.INFOSHINA_THEME_BUY_BUTTON_ON_MODEL_TEMPLATE
    return template


@register.simple_tag
def product_not_available_html():
    return settings.INFOSHINA_THEME_PRODUCT_NOT_AVAILABLE_TEMPLATE


@register.simple_tag
def buy_on_credit_button():
    if settings.INFOSHINA_THEME_SHOW_CREDIT_BUTTONS:
        return settings.INFOSHINA_THEME_BUY_ON_CREDIT_BUTTON_TEMPLATE
    return ''


@register.simple_tag
def credit_processing_button():
    if settings.INFOSHINA_THEME_SHOW_CREDIT_BUTTONS:
        return settings.INFOSHINA_THEME_CREDIT_PROCESSING_BUTTON_TEMPLATE
    return ''


@register.simple_tag(takes_context=True)
def all_variants_button(context, pk):
    return render_meta_info(
        settings.INFOSHINA_THEME_ALL_VARIANTS_BUTTON_TEMPLATE,
        {
            'link': reverse(
                'lfs_product_variants_for_category',
                kwargs={'product_id': pk}),
            'pk': pk,
            'page_mode': context.get('page_mode'),
            'product': context.get('product'),
        })


@register.simple_tag(takes_context=True)
def insert_buy_button_to_all_variants(context):
    from django.template.loader import render_to_string
    from django.template import RequestContext
    request = context.get('request')
    buy_button_html = render_to_string(
            'lfs/catalog/buy_button.html', RequestContext(request, {
                'page_type': 'model_page'}))
    return buy_button_html


ESENDER_TEMPLATE_START = '''
{% load url from future %}
'''
ESENDER_TEMPLATE_END = '''
<script type="text/javascript">
    $("#subscribe_abonent_submit_{{ portlet_id }}").click(function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url: '{% url "esender_subscribe_abonent" %}',
            data: $('#subscribe_abonent_{{ portlet_id }}').serialize()+'&use_captcha={{use_captcha}}&portlet_id={{ portlet_id }}&popup={{ popup }}&page_type={{ page_type }}',
            success:function (data){
                data = JSON.parse(data);
                $('#esender_subscribe_abonent_block_{{ portlet_id }}').html(data['html']);
            }
        });
    });
    {% if message %}
        $('#esender_subscribe_abonent_block_{{ portlet_id }}').html('<div class="esender-wrapper"><p class="esender-success-text">{{message}}</></div>');
    {% endif %}
    {% if slot_name == "Left" %}config.left_portlet_count++;{% endif %}
    {% if slot_name == "Right" %}config.right_portlet_count++;{% endif %}
</script>
'''


@register.assignment_tag(takes_context=True)
def esender_tempalte(context):
    return render_meta_info(
        ESENDER_TEMPLATE_START +
        settings.INFOSHINA_THEME_ESENDER_TEMPLATE +
        ESENDER_TEMPLATE_END,
        context
    )


@register.assignment_tag(takes_context=True)
def esender_product_template(context):
    return render_meta_info(
        ESENDER_TEMPLATE_START +
        settings.INFOSHINA_THEME_ESENDER_PRODUCT_TEMPLATE +
        ESENDER_TEMPLATE_END,
        context
    )


@register.assignment_tag(takes_context=True)
def property_for_product_infoshina(context, product, property_identifier):
    return _get_property_value(product, property_identifier)


@register.assignment_tag(takes_context=True)
def shipping_data(context):
    from lfs.shipping.utils import (
        get_selected_shipping_method,
        get_shipping_costs,
        )
    request = context.get('request')
    if request:
        selected_shipping_method = get_selected_shipping_method(request)
        shipping_costs = get_shipping_costs(request, selected_shipping_method)
        return {
                'price': shipping_costs['price'],
                'shipping_method': selected_shipping_method
            }
    return {
        'price': 0,
        'shipping_method': None,
        }


@register.filter
def add_price(price, price_to_add):
    return price + price_to_add


@register.simple_tag(takes_context=True)
def one_click_order_button_template(context):
    return render_meta_info(
        settings.INFOSHINA_THEME_ONE_CLICK_ORDER_BUTTON_TEMPLATE,
        {
            'product_id': context.get('product_id'),
            'is_set': context.get('is_set'),
            'params': context.get('params'),
        }
    )


@register.assignment_tag
def get_manufacturer_by_filter(request_get, category):
    from lfs.manufacturer.models import Manufacturer
    if request_get and 'brand' in request_get.keys() and\
            len(request_get['brand'].split(',')) == 1:
        print category
        try:
            manufacturer = Manufacturer.objects.get(
                name=FilterOption.objects.get(
                    identificator=request_get['brand'],
                    filter__category=category).title)
        except:
            manufacturer = None
        return manufacturer
    return None


@register.assignment_tag(takes_context=True)
def get_seo_text_new_infoshina(context):
    from lfs.core.utils import import_symbol
    request = context.get("request")
    seo_generators = getattr(settings, 'SEO_DATA_GENERATORS')
    seo_generators = sorted(
        seo_generators,
        key=lambda tup: tup[1], reverse=True)
    seo_text = None
    for generator_url in seo_generators:
        generator = import_symbol(generator_url[0])
        obj = generator(request, context)
        seo_text = obj.get_seo_text()
        if seo_text:
            break
    if seo_text:
        return seo_text
    else:
        return ''


@register.filter
def tetradka_replace_words(product_name):
    for line in settings.TETRADKA_REPLACED_WORDS.split('\n'):
        if '###' not in line:
            line += '###'
        product_name = product_name.replace(
            line.split('###')[0], line.split('###')[1])
    return product_name


@register.inclusion_tag('reviews/reviews_for_instance.html',
                        takes_context=True)
def reviews_for_instance_infoshina(context, instance):
    from lfs.catalog.settings import VARIANT, PRODUCT_WITH_VARIANTS
    from reviews import utils as reviews_utils
    from reviews.models import Review
    from django.contrib.contenttypes.models import ContentType
    if instance.sub_type == VARIANT:
        ids = [v['id'] for v in instance.parent.get_variants().values('id')]
        ids.append(instance.parent.id)
    elif instance.sub_type == PRODUCT_WITH_VARIANTS:
        ids = [v['id'] for v in instance.get_variants().values('id')]
        ids.append(instance.id)
    else:
        ids = [instance.id]
    request = context.get("request")
    ctype = ContentType.objects.get_for_model(instance)
    has_rated = reviews_utils.has_rated(request, instance)
    reviews = Review.objects.active().filter(
        content_type=ctype.id, content_id__in=ids)

    return {
        "reviews": reviews,
        "has_rated": has_rated,
        "content_id": instance.id,
        "content_type_id": ctype.id,
        "MEDIA_URL": context.get("MEDIA_URL")
    }


@register.assignment_tag
def get_product_slug(product_id):
    try:
        return Product.objects.get(id=product_id).slug
    except:
        return ''


@register.assignment_tag
def get_car_name(request):
    from stepbystep_search.models import StepItem
    slug_attribute = getattr(request, 'proxyattr_car', False)
    if not slug_attribute:
        return ''
    try:
        si = StepItem.objects.get(slug=slug_attribute)
        return si.name
    except:
        return ''
