# coding: utf-8
from django.conf.urls.defaults import *

urlpatterns =\
    patterns(
        'new_infoshina_theme.views',
        url(r'^get-products-statuses/$',
            'get_products_statuses'
            ),
        url(r'^get-product-seo-text$',
            'get_product_seo_text',
            name='get_product_seo_text'),
        url(r'^modal-registration/$',
            'modal_registration',
            name='modal_registration'),
        url(r'^modal-login/$',
            'modal_login',
            name='modal_login'),
        url(r'^notebook-create-cart$',
            'notebook_add_to_cart',
            name='notebook_add_to_cart'),
        url(r'^notebook-remove-checke-products$',
            'notebook_remove_checked_products',
            name='notebook_remove_checked_products'),
        url(r'^notebook-set-page-mode$',
            'notebook_set_page_mode',
            name='notebook_set_page_mode')
    )
