from distutils.core import setup
from setuptools import find_packages

setup(
    name='new_infoshina_theme',
    version='4.2.8',
    author='Anna Gordienko',
    author_email='annagordienko7@gmail.com',
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
    entry_points={
        'sshop.plugins.settings.entrypoints': [
            'update_settings = new_infoshina_theme:update_settings',
            'update_urls = new_infoshina_theme:update_urls',
            'install = new_infoshina_theme:install',
            'upgrade = new_infoshina_theme:upgrade',
        ],
    },
    install_requires=[
        'cssselect==0.9.1',
    ],
)
